
import view.VistaPrincipal;

/**
 * Persistencia Proyectos Construcción
 *
 */
public class App {
    public static void main(String[] args) {
        VistaPrincipal objVista = new VistaPrincipal();
        objVista.setVisible(true);
    }
}
