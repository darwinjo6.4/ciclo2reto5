package model.dao;

//Estructura de datos
import java.util.ArrayList;

import model.vo.Lider;
import model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import util.JDBCUtilities;

public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {
        // Arreglo
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        try {
            // Conexion a base y ejecucion de query
            Connection conexion = JDBCUtilities.getConnection();
            String query = "SELECT Fecha_Inicio, Numero_Habitaciones, Numero_Banos FROM Proyecto p WHERE Constructora = 'Pegaso'";
            PreparedStatement pStatement = conexion.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            // Crear cada objeto y añadir al array
            while (resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(resultado.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(resultado.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banios(resultado.getInt("Numero_Banos"));
                proyectos.add(objProyecto);
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return proyectos;
    }// Fin del método query_requerimiento_1


    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        // Arreglo
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        try {
            // Conexion a base y ejecucion de query
            Connection conexion = JDBCUtilities.getConnection();
            String query = "SELECT p.Fecha_Inicio, p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato FROM Proyecto p INNER JOIN Lider l ON l.ID_Lider = p.ID_Lider INNER JOIN Tipo t ON t.ID_Tipo = p.ID_Tipo WHERE p.Constructora = 'Pegaso'";
            PreparedStatement pStatement = conexion.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            // Crear objeto y añadir al array
            while (resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setFecha_inicio(resultado.getString("Fecha_Inicio"));
                objProyecto.setNum_habitaciones(resultado.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banios(resultado.getInt("Numero_Banos"));
                objProyecto.setEstrato_proyecto(resultado.getInt("Estrato"));
                Lider objLider = new Lider(resultado.getString("Nombre"), resultado.getString("Primer_Apellido"));
                objProyecto.setLider(objLider);
                proyectos.add(objProyecto);
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return proyectos;
    }// Fin del método query_requerimiento_2

    
    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        // Arreglo
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        try {
            // Conexion a base y ejecucion de query
            Connection conexion = JDBCUtilities.getConnection();
            String query = "SELECT SUM(p.Numero_Habitaciones) AS 'Total_Habitaciones', p.Constructora FROM Proyecto p GROUP BY p.Constructora";
            PreparedStatement pStatement = conexion.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            // Crear objeto y añadir al array
            while (resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_habitaciones(resultado.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(resultado.getString("Constructora"));
                proyectos.add(objProyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return proyectos;
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        // Arreglo
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        try {
            // Conexion a base y ejecucion de query
            Connection conexion = JDBCUtilities.getConnection();
            String query = "SELECT SUM(p.Numero_Habitaciones) AS 'Total_Habitaciones', p.Constructora FROM Proyecto p GROUP BY p.Constructora HAVING Total_Habitaciones > 200 ORDER BY Total_Habitaciones ASC";
            PreparedStatement pStatement = conexion.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            // Crear objeto y añadir al array
            while (resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_habitaciones(resultado.getInt("Total_Habitaciones"));
                objProyecto.setNombre_constructora(resultado.getString("Constructora"));
                proyectos.add(objProyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return proyectos;
    }// Fin del método query_requerimiento_5

}