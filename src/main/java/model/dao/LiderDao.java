package model.dao;

//Estructura de datos
import java.util.ArrayList;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import util.JDBCUtilities;

//Encapsulamiento de los datos
import model.vo.Lider;

public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        ArrayList<Lider> lideres = new ArrayList<Lider>();
        try {
            Connection conexion = JDBCUtilities.getConnection();
            String query = "SELECT l.Nombre AS 'Nombre', l.Primer_Apellido AS 'Apellido' FROM Lider l INNER JOIN Proyecto p ON l.ID_Lider = p.ID_Lider WHERE p.Constructora = 'Pegaso'";
            PreparedStatement pStatement = conexion.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            // Crear cada objeto y añadir al array
            while (resultado.next()){
                Lider objLider = new Lider(resultado.getString("Nombre"), resultado.getString("Apellido"));
                lideres.add(objLider);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return lideres;
    }
}