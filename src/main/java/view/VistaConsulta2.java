package view;

import java.util.ArrayList;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import controller.Controlador;
import model.vo.Proyecto;

public class VistaConsulta2 extends JFrame{
    // Atributos
    public static final Controlador controlador = new Controlador();
    private DefaultTableModel modelo;
    private JTable tabla;
    private JButton btnAtras;

    // Constructor
    public VistaConsulta2(){
        String[] colNames = {"Fecha_Inicio", "Numero_Habitaciones", "Numero_Banos", "Nombre_Lider", "Apellido_Lider", "Estrato"};
        String[] data = new String[6];
        this.modelo = new DefaultTableModel(null, colNames);

        try{
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            for (int i=0; i < proyectos.size(); i++){
                data[0] = proyectos.get(i).getFecha_inicio();
                data[1] = String.valueOf(proyectos.get(i).getNum_habitaciones());
                data[2] = String.valueOf(proyectos.get(i).getNum_banios());
                data[3] = proyectos.get(i).getLider().getNombre();
                data[4] = proyectos.get(i).getLider().getApellido();
                data[5] = String.valueOf(proyectos.get(i).getEstrato_proyecto());
                modelo.addRow(data);
            }
        } catch (Exception e){
            System.err.println(e);
        }

        this.tabla = new JTable(modelo);

        // Panel
        this.setTitle("Constructora");
        this.setBounds(530, 230, 550, 400);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        //Scroll bar
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 10, 500, 300);
        this.getContentPane().add(scrollPane);
        scrollPane.setViewportView(tabla);

        //Boton
        this.btnAtras = new JButton("Regresar");

        btnAtras.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                    VistaPrincipal objVistaPrincipal = new VistaPrincipal();
                    objVistaPrincipal.setVisible(true);
                    dispose();
                }
            }
        );

        // dimensiones y posición del botón
        btnAtras.setBounds(210, 315, 100, 40);
        // pongo el botón en la ventana
        getContentPane().add(btnAtras);
    }
}
