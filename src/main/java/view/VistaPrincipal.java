package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class VistaPrincipal extends JFrame {
    // Atributos
    private JLabel lblConsulta1a;
    private JLabel lblConsulta2a;
    private JLabel lblConsulta3a;
    private JLabel lblConsulta4a;
    private JLabel lblConsulta5a;

    private JLabel lblConsulta1b;
    private JLabel lblConsulta2b;
    private JLabel lblConsulta3b;
    private JLabel lblConsulta4b;
    private JLabel lblConsulta5b;

    private JButton btnConsulta1;
    private JButton btnConsulta2;
    private JButton btnConsulta3;
    private JButton btnConsulta4;
    private JButton btnConsulta5;

    public VistaPrincipal(){
        this.setTitle("Constructora");
        this.setBounds(530, 230, 550, 400);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(new BorderLayout(5, 5));

        // Contenedor
        GridLayout lLayout = new GridLayout(5, 1, 5, 5);
        GridLayout cLayout = new GridLayout(5, 1, 5, 5);
        GridLayout rLayout = new GridLayout(5, 1, 45, 45);
        
        Container lContainer = new Container();
        Container cContainer = new Container();
        Container rContainer = new Container();

        lContainer.setLayout(lLayout);
        cContainer.setLayout(cLayout);
        rContainer.setLayout(rLayout);

        // Objetos
        this.lblConsulta1a = new JLabel("Consulta 1");
        this.lblConsulta1b = new JLabel("Fecha inicio / No. Habitaciones / No. Baños", SwingConstants.CENTER);
        lblConsulta1b.setFont(new Font("Serif", Font.TYPE1_FONT, 12));
        this.btnConsulta1 = new JButton("Consultar");

        this.lblConsulta2a = new JLabel("Consulta 2");
        this.lblConsulta2b = new JLabel("<html>Fecha inicio / No. Habitaciones / No. Baños<br/> / Nombre Lider / Apellido Lider / Estrato</html>", SwingConstants.CENTER);
        lblConsulta2b.setFont(new Font("Serif", Font.TYPE1_FONT, 12));
        this.btnConsulta2 = new JButton("Consultar");

        this.lblConsulta3a = new JLabel("Consulta 3");
        this.lblConsulta3b = new JLabel("Total habitaciones / Constructora", SwingConstants.CENTER);
        lblConsulta3b.setFont(new Font("Serif", Font.TYPE1_FONT, 12));
        this.btnConsulta3 = new JButton("Consultar");

        this.lblConsulta4a = new JLabel("Consulta 4");
        this.lblConsulta4b = new JLabel("Nombre Lider / Apellido Lider", SwingConstants.CENTER);
        lblConsulta4b.setFont(new Font("Serif", Font.TYPE1_FONT, 12));
        this.btnConsulta4 = new JButton("Consultar");

        this.lblConsulta5a = new JLabel("Consulta 5");
        this.lblConsulta5b = new JLabel("Total habitaciones > 200 / Constructora", SwingConstants.CENTER);
        lblConsulta5b.setFont(new Font("Serif", Font.TYPE1_FONT, 12));
        this.btnConsulta5 = new JButton("Consultar");

        // Acciones de botones
        btnConsulta1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                VistaConsulta1 objVistaConsulta1 =  new VistaConsulta1();
                objVistaConsulta1.setVisible(true);
                dispose();
            }
        });

        btnConsulta2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                VistaConsulta2 objVistaConsulta2 =  new VistaConsulta2();
                objVistaConsulta2.setVisible(true);
                dispose();
            }
        });

        btnConsulta3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                VistaConsulta3 objVistaConsulta3 =  new VistaConsulta3();
                objVistaConsulta3.setVisible(true);
                dispose();
            }
        });

        btnConsulta4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                VistaConsulta4 objVistaConsulta4 =  new VistaConsulta4();
                objVistaConsulta4.setVisible(true);
                dispose();
            }
        });

        btnConsulta5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                VistaConsulta5 objVistaConsulta5 =  new VistaConsulta5();
                objVistaConsulta5.setVisible(true);
                dispose();
            }
        });

        // Agregar elementos
        lContainer.add(this.lblConsulta1a);
        cContainer.add(this.lblConsulta1b);
        rContainer.add(this.btnConsulta1);
        lContainer.add(this.lblConsulta2a);
        cContainer.add(this.lblConsulta2b);
        rContainer.add(this.btnConsulta2);
        lContainer.add(this.lblConsulta3a);
        cContainer.add(this.lblConsulta3b);
        rContainer.add(this.btnConsulta3);
        lContainer.add(this.lblConsulta4a);
        cContainer.add(this.lblConsulta4b);
        rContainer.add(this.btnConsulta4);
        lContainer.add(this.lblConsulta5a);
        cContainer.add(this.lblConsulta5b);
        rContainer.add(this.btnConsulta5);

        // Añadir a la ventana principal
        this.add(lContainer, BorderLayout.WEST);
        this.add(cContainer, BorderLayout.CENTER);
        this.add(rContainer, BorderLayout.EAST);

    }
}
