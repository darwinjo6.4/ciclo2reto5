package view;

import java.util.ArrayList;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import controller.Controlador;
import model.vo.Lider;

public class VistaConsulta4 extends JFrame{
    // Atributos
    public static final Controlador controlador = new Controlador();
    private DefaultTableModel modelo;
    private JTable tabla;
    private JButton btnAtras;

    // Constructor
    public VistaConsulta4(){
        String[] colNames = {"Nombre", "Apellido"};
        String[] data = new String[2];
        this.modelo = new DefaultTableModel(null, colNames);

        try{
            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for (int i=0; i < lideres.size(); i++){
                data[0] = lideres.get(i).getNombre();
                data[1] = lideres.get(i).getApellido();
                modelo.addRow(data);
            }
        } catch (Exception e){
            System.err.println(e);
        }

        this.tabla = new JTable(modelo);

        // Panel
        this.setTitle("Constructora");
        this.setBounds(530, 230, 550, 400);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        //Scroll bar
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 10, 500, 300);
        this.getContentPane().add(scrollPane);
        scrollPane.setViewportView(tabla);

        //Boton
        this.btnAtras = new JButton("Regresar");

        btnAtras.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                    VistaPrincipal objVistaPrincipal = new VistaPrincipal();
                    objVistaPrincipal.setVisible(true);
                    dispose();
                }
            }
        );

        // dimensiones y posición del botón
        btnAtras.setBounds(210, 315, 100, 40);
        // pongo el botón en la ventana
        getContentPane().add(btnAtras);
    }
}
